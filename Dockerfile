FROM archlinux/base

RUN pacman -Syu --noconfirm
RUN pacman -Sy --noconfirm nodejs rtorrent screen git npm python clang make sudo nginx
RUN useradd -m -u 500 -U rtorrent
RUN useradd -m -u 505 -U flood
RUN useradd -m -u 510 -U nginx

COPY nginx/nginx.conf /etc/nginx/nginx.conf

RUN git clone https://github.com/Flood-UI/flood.git /home/flood/ui
RUN cd /home/flood/ui && npm install
COPY flood/config.js /home/flood/ui/config.js
RUN cd /home/flood/ui && npm run build
RUN chown -R flood: /home/flood

COPY rtorrent/.rtorrent.rc /home/rtorrent/.rtorrent.rc
COPY entrypoint.sh /root/entrypoint.sh

ENTRYPOINT /root/entrypoint.sh