#!/bin/bash
set -e
set -x

if [ ! -d "/rtorrent_data" ]; then
    mkdir /rtorrent_data
fi

if [ ! -d "/rtorrent_data/.session" ]; then
    mkdir /rtorrent_data/.session
fi

if [ -f "/rtorrent_data/.session/rtorrent.lock" ]; then
    rm /rtorrent_data/.session/rtorrent.lock
fi

if [ ! -d "/downloads" ]; then
    mkdir /downloads
fi

if [ ! -d "/flood_data" ]; then
    mkdir /flood_data
fi

if [ ! -z "$RUID" ]; then
    if [ -z "$FUID" ]; then
        usermod -ou $RUID flood
    fi
    usermod -ou $RUID rtorrent
fi

if [ ! -z "$RGID" ]; then
    if [ -z "$FGID" ]; then
        groupmod -og $RGID flood
    fi
    groupmod -og $RGID rtorrent
fi

if [ ! -z "$FUID" ]; then
    usermod -ou $FUID flood
fi
if [ ! -z "$FGID" ]; then
    groupmod -og $FGID flood
fi

chown -R rtorrent:rtorrent /home/rtorrent
chown -R rtorrent:rtorrent /rtorrent_data
chown -R rtorrent:rtorrent /downloads

find /home/flood -path /home/flood/ui/node_modules -prune -o -print0 | xargs -n 1 -P 100 -0 chown flood:flood
chown -R flood:flood /flood_data

/usr/bin/nginx -g "daemon off;" &
sudo -u rtorrent bash -c "cd /home/rtorrent && /usr/bin/screen -d -m -fa -S rtorrent /usr/bin/rtorrent" &
sudo -u flood bash -c "cd /home/flood/ui && NODE_ENV=production /usr/bin/npm start" 